const { forEach } = require('lodash');

console.log('Core start');

console.log('We have following components in window:');

forEach(window.__COMPONENTS, (component, componentName) => {
  console.log(`Component name: ${componentName}, component: `, component);
});

console.log('Lets build page from that components ...');
console.log('TODO');