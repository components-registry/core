const path = require('path');

module.exports = {
  entry: path.join(__dirname, './core/index.js'),
  output: {
    filename: "build.js",
    path: __dirname + "/core"
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".json"]
  },
};